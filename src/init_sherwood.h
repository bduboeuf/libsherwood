#ifndef _INIT_SHERWOOD_H_
#define _INIT_SHERWOOD_H_


#include <hdf5.h>
#include "./sherwood_types.h"


hid_t init_sherwood(char*, sherwood_t*);


#endif /* _INIT_SHERWOOD_H_ */
