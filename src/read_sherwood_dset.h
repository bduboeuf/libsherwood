#ifndef _READ_SHERWOOD_DSET_H_
#define _READ_SHERWOOD_DSET_H_


#include <hdf5.h>
#include "./sherwood_types.h"


int read_sherwood_dset(char*, enum _PTypes, char*, hid_t, void*);


#endif /* _READ_SHERWOOD_DSET_H_ */
