/*
* read_sherwood_dset.c
* tests_file: read_sherwood_dset_tests.c
*
* Read an SHERWOOD dataset
*
* @param: fmt_path: Formatted snapshot path
* @param: ptype: Particle type
* @param: dset_name:
* @param: dtype_id: Data type identifier
* @param: buf: Pointer to the allocated space for loading the dataset
*
* @return: EXIT_FAILURE or EXIT_SUCCESS
*
* TODO: to be refactored
*/


#include <stdlib.h>
#include <string.h>
#include <High5.h>
#include "./read_sherwood_dset.h"


#define MAXDIM (2)  /* Maximum availabl: 32 */

#ifndef PRINT
#define PRINT printf
#endif


int read_sherwood_dset(char* fmt_path, enum _PTypes ptype, char *dset_name,
  hid_t dtype_id, void *buf)
  {
    if(strstr(fmt_path, "%d") == NULL) {
      printf("[Warning] Please use a formatted file path.\n");
      printf("          e.g. /path/to/sherwood/snap.%%d.h5\n");
      return EXIT_FAILURE;
    }

    char path[1024], dset_path[1024];
    sprintf(path, fmt_path, 0);
    sprintf(dset_path, "PartType%d/%s", ptype, dset_name);

    int num_files;
    hid_t fid = open_h5(path, H5F_ACC_RDONLY, H5P_DEFAULT);
    read_h5attr(fid, "Header", "NumFilesPerSnapshot", H5T_NATIVE_INT, &num_files);
    close_h5(fid);

    hsize_t dims[MAXDIM];
    int ndims, buf_offset = 0;

    hsize_t start[MAXDIM], count[MAXDIM];

    for(int ifile = 0; ifile < num_files; ifile++)
    {
      sprintf(path, fmt_path, ifile);
      fid = open_h5(path, H5F_ACC_RDONLY, H5P_DEFAULT);

      get_h5dset_dims(fid, dset_path, &ndims, dims);

      for(int i = 1; i < ndims; i++)
      {
        count[i] = dims[i];
        start[i] = 0;
      }

      buf = (char*)buf + H5Tget_size(dtype_id) * buf_offset;

      read_h5dset(fid, dset_path, dtype_id, buf);

      buf_offset = 1;
      for(int j = 0; j < ndims; j++)
        buf_offset *= (int)dims[j];

      close_h5(fid);
    }

    return EXIT_SUCCESS;
  }
