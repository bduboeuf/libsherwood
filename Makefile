MODULE_NAME := libsherwood

MAJOR := 0
MINOR := 0
PATCH := 0


OBJS := init_sherwood.o \
        read_sherwood_dset.o \
 #       init_hash.o \
 #       peano_hilbert_key.o \
 #       crop_sherwood.o \
 #       count_particles.o


FORTRAN_OBJS := sherwood_types_f.o\
                libsherwood.o


TEST_OBJS := init_sherwood_tests.o \
             read_sherwood_dset_tests.o \
#             init_hash_tests.o \
#             crop_sherwood_tests.o \
#             count_particles_tests.o


#INCLUDES := -I ${HOME}/.local/include
#LIBS := -lHigh5 -lhdf5

INCLUDES := -I /usr/include/hdf5/serial
LIBS := -lHigh5 -lhdf5 -L /usr/lib/hdf5/serial

include ./Makefile.paCage/Makefile
